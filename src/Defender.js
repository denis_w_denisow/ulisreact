import React from 'react';
import App from './App'
import LoginContainer from './components/Login/LoginContainer';



const Defender = () => {
  
  let authorizedInLocalStorage = localStorage.authorized;
  if (authorizedInLocalStorage) {
    return <App />
  }

  return <LoginContainer />
}

export default Defender;
