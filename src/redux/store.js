import { createStore, combineReducers, applyMiddleware } from 'redux';
import preloaderReducer from './reducers/common/preloaderReducer';
import applicationsReducer from './reducers/applicationsReducer';
import workersReducer from './reducers/workersReducer';
import workerReducer from './reducers/workerReducer';
import cabinetsReducer from './reducers/cabinetsReducer';
import loginReducer from './reducers/loginReducer';
import menuReducer from './reducers/menuReducer';
import graphicsReducer from './reducers/graphicsReducer';
import { reducer as formReducer} from 'redux-form';
import thunkMiddleware from 'redux-thunk';

let reducers = combineReducers({  
  preloaderReducer,
  applicationsReducer,
  workersReducer,
  workerReducer,
  cabinetsReducer,
  loginReducer,
  menuReducer,
  form: formReducer,
  graphicsReducer,
});

let store = createStore(reducers, applyMiddleware(thunkMiddleware));

window.store = store;


export default store;