export const getGraphics = (state) => {
  return state.graphicsReducer.graphics
}

export const getTotal = (state) => {
  return state.graphicsReducer.total
}