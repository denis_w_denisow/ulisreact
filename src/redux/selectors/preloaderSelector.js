export const getPreloaderCondition = (state) => {
  return state.preloaderReducer.loaderCondition;
}