export const getWorkers = (state) => {
  return state.workersReducer.workers
}

export const getTotal = (state) => {
  return state.workersReducer.total
}

export const getCurrentPage = (state) => {
  return state.workersReducer.currentPage
}