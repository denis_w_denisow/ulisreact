export const getApplications = (state) => {
  return state.applicationsReducer.applications
}

export const getTotalRecords = (state) => {
  return state.applicationsReducer.total
}