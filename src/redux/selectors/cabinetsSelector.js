export const getCabinets = (state) => {
  return state.cabinetsReducer.cabinets
}

export const getTotal = (state) => {
  return state.cabinetsReducer.total
}