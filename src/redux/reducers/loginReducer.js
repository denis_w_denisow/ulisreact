import { loginApi, getUserProfileApi } from '../../api/login'

const SET_LOGIN = 'SET_LOGIN';
const SET_LOGIN_INFORMATION = 'SET_LOGIN_INFORMATION';
const SET_AUTHORIZED = 'SET_AUTHORIZED';

let initialState = {
  email: null,
  password: null,
  access_token: null,
  refresh_token: null,
  token_type: null,
  expires_in: null,
  authorized: false
};

const loginReducer = (state = initialState, action) => {

  switch (action.type) {
    case SET_LOGIN:
      return { ...state, email: action.login.email, password: action.login.password };
    case SET_LOGIN_INFORMATION:
      return { 
        ...state, 
        access_token: action.information.access_token,
        refresh_token: action.information.refresh_token,
        token_type: action.information.token_type,
        expires_in: action.information.expires_in,
      };
      case SET_AUTHORIZED:
        return { ...state, authorized: action.condition };

    default:
      return state;
  }

}

export const setLogin = (login) => ({ type: SET_LOGIN, login });
export const setLoginInformation = (information) => ({ type: SET_LOGIN_INFORMATION, information });
export const setAuthorized = (condition) => ({ type: SET_AUTHORIZED, condition });

export const getAccessToken = (email, password) => {
  return (dispatch) => { 
    dispatch(setLogin({email, password}));
    loginApi(email, password)
      .then((response) => {
        if(response.status === 200) {
          localStorage.setItem('access_token', response.data.access_token);
          localStorage.setItem('refresh_token', response.data.refresh_token);
          localStorage.setItem('expires_in', response.data.expires_in);
          dispatch(setLoginInformation(response.data));
          localStorage.setItem('authorized', true);
          dispatch(setAuthorized(true));
          getUserProfileApi(response.data.access_token)
            .then((profile) => {
              console.log(profile.data.worker);
              localStorage.setItem('worker', JSON.stringify(profile.data.worker));
              console.log(JSON.parse(localStorage.getItem('worker')));
              window.document.location.reload();
            })          
        }      
      });
  }
}

export const logout = () => {
  localStorage.clear();
  window.document.location.href = '/';
}

export default loginReducer;