import { apiGetData } from '../../api/applications';

const SET_APPLICATIOS = 'applications/SET_APPLICATIOS';
const SET_TOTAL_RECORDS = 'applications/SET_TOTAL_RECORDS';

let initialState = {
  applications: [
    {
      "id": 0,
      "old_id": 0,
      "old_reestr": 0,
      "office_id": 0,
      "service_id": 0,
      "application_condition_id": 0,
      "registration_number": "",
      "order_count": 0,
      "created_at": "2019-05-16T21:00:00.000000Z",
      "updated_at": "2020-08-12T11:28:22.000000Z",
      "deleted_at": null,
      "details": {
        "application_id": 0,
        "registration_name": "",
        "ticket": "",
        "object_count": "",
        "note": "",
        "description": ""
      },
      "current_condition": {
        "id": 0,
        "application_id": 0,
        "status_id": 0,
        "conversion_id": 0,
        "worker_id": 0,
        "cabinet_id": 0,
        "registry_id": 0,
        "created_at": "2019-06-18T21:00:00.000000Z",
        "deleted_at": null,
        "status": {
          "id": 0,
          "name": "",
          "css_name": "default",
          "created_at": "2020-09-02T07:56:22.000000Z",
          "updated_at": "2020-09-02T07:56:22.000000Z",
          "deleted_at": null
        },
        "conversion": {
          "id": 0,
          "name": "",
          "css_name": "default",
          "created_at": null,
          "updated_at": null,
          "deleted_at": null
        },
        "cabinet": {
            "id": 0,
            "office_id": 0,
            "name": "",
            "address": "",
            "created_at": "2020-09-02T07:55:42.000000Z",
            "updated_at": "2020-09-02T07:55:42.000000Z",
            "deleted_at": null
        }
      },
      "service": {
        "id": 0,
        "office_id": 0,
        "parent_id": 0,
        "full_name": "",
        "short_name": "",
        "created_at": null,
        "updated_at": null,
        "deleted_at": null
      }
    },
  ],
  total: 0,
};

const applicationsReducer = (state = initialState, action) => {

  switch (action.type) {
    case SET_APPLICATIOS:
      return { ...state, applications: [...action.applications] };
    case SET_TOTAL_RECORDS:
      return { ...state, total: action.total };

    default:
      return state;
  }
}

export const setApplications = (applications) => ({ type: SET_APPLICATIOS, applications });

export const setTotalRecords = (total) => ({ type: SET_TOTAL_RECORDS, total });

export const requestApplications = (search, countPage, currentPage) => async (dispatch) => {
  //let response = await apiGetApplications(countPage, currentPage);
  let start = currentPage * countPage - countPage;
  let response = await apiGetData(search, countPage, start);
  dispatch(setApplications(response.data.data));
  dispatch(setTotalRecords(response.data.recordsFiltered));
}

export default applicationsReducer;