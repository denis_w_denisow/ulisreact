import { apiGetGraphics } from '../../api/graphics';

const SET_GRAPHICS = 'graphics/SET_GRAPHICS';
const SET_TOTAL_RECORDS = 'graphics/SET_TOTAL_RECORDS';

let initialState = {
  graphics: [],
  total: 0,
};

const graphicsReducer = (state = initialState, action) => {

  switch (action.type) {
    case SET_GRAPHICS:
      return { ...state, graphics: [...action.graphics] };
    case SET_TOTAL_RECORDS:
      return { ...state, total: action.total};

    default:
      return state;
  }
}

export const setGraphics = (graphics) => ({ type: SET_GRAPHICS, graphics });

export const setTotalRecords = (total) => ({ type: SET_TOTAL_RECORDS, total });

export const requestGraphics = (countPage, currentPage) => async (dispatch) => {
  let data = await apiGetGraphics(countPage, currentPage)
  dispatch(setGraphics(data.data));
  dispatch(setTotalRecords(data.total));
}

export default graphicsReducer;