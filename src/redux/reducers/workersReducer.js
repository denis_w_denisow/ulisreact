import { apiGetWorkers } from '../../api/workers';

const SET_WORKERS = 'workers/SET_WORKERS';
const SET_TOTAL_RECORDS = 'workers/SET_TOTAL_RECORDS';
const SET_CURRENT_PAGE = 'workers/SET_CURRENT_PAGE';

let initialState = {
  workers: [],
  total: 0,
  currentPage: 1,
};

const workersReducer = (state = initialState, action) => {

  switch (action.type) {
    case SET_WORKERS:
      return { ...state, workers: [...action.workers] };
    case SET_TOTAL_RECORDS:
      return { ...state, total: action.total };
    case SET_CURRENT_PAGE:
      return { ...state, currentPage: action.currentPage}

    default:
      return state;
  }

}


export const setWorkers = (workers) => ({ type: SET_WORKERS, workers });

export const setTotalRecords = (total) => ({ type: SET_TOTAL_RECORDS, total });

export const setCurrentPage = (currentPage) => ({ type: SET_CURRENT_PAGE, currentPage });

export const requestWorkers = (countPage, currentPage) => {
  return async (dispatch) => {
    let response = await apiGetWorkers(countPage, currentPage);
    dispatch(setWorkers(response.data));
    dispatch(setTotalRecords(response.total));
  }
}

export default workersReducer;