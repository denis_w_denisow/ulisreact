import { apiGetMenu } from '../../api/menu';

const SET_MENU = 'menu/SET_MENU';

let initialState = {
  menu: [],
};

const menuReducer = (state = initialState, action) => {

  switch (action.type) {
    case SET_MENU:
      return { ...state, menu: [...action.menu] };

    default:
      return state;
  }
}

export const setMenu = (menu) => ({ type: SET_MENU, menu });

export const requestMenu = () => async (dispatch) => {
  let response = await apiGetMenu()
  dispatch(setMenu(response.data));
}

export default menuReducer;