import { apiGetCabinets, apiSaveCabinetAddress } from '../../api/cabinets';


const SET_CABINETS = 'cabinets/SET_CABINETS';
const SET_CABINET_ADDRESS = 'cabinets/SET_CABINET_ADDRESS';
const SET_TOTAL_RECORDS = 'cabinets/SET_TOTAL_RECORDS';

let initialState = {
  cabinets: [],
  total: 0,
};

const cabinetsReducer = (state = initialState, action) => {

  switch (action.type) {
    case SET_CABINETS:
      return { ...state, cabinets: [...action.cabinets] };
    case SET_CABINET_ADDRESS:
      let changedCabinets = state.cabinets.map((cabinet) => {
        let address = cabinet.address;
        if (cabinet.id === action.cabinetId) address = action.address;
        return { ...cabinet, address: address };
      });
      return { ...state, cabinets: [...changedCabinets] };
    case SET_TOTAL_RECORDS:
      return { ...state, total: action.total };

    default:
      return state;
  }

}

export const setCabinets = (cabinets) => ({ type: SET_CABINETS, cabinets });

export const setCabinetAddress = (cabinetId, address) => ({ type: SET_CABINET_ADDRESS, cabinetId, address });

export const setTotalRecords = (total) => ({ type: SET_TOTAL_RECORDS, total });

export const saveCabinetAddress = (cabinetId, address) => async (dispatch) => {
    await apiSaveCabinetAddress(cabinetId, address);
}

export const requestCabinets = (countPage, currentPage) => async (dispatch) => {
  let data = await apiGetCabinets(countPage, currentPage);
  dispatch(setCabinets(data.data));
  dispatch(setTotalRecords(data.total));
}

export default cabinetsReducer;