import { apiGetWorker } from '../../api/workers';

const SET_WORKER = 'worker/SET_WORKER';

let initialState = {
  worker: {
    id: 0,
    surname: '',
    name: '',
    patronymic: '',
    user_statuse_id: 0,
    user_status: { 
      id: 1, 
      name: '', 
      css_name: '' 
    },
    office_id: 0,
    office: {
      id: 0,
      organisation_id: 0,
      short_name: '',
      full_name: '',
      address: '',
      email: null,
      cloud_link: null,
  },
    cabinet_id: 0,
    cabinet: {
      id: 0,
      office_id: 0,
      name: '',
      address: ''
  },
    position_id: 0,
    position: {
      id: 0,
      name: '',
    },
    created_at: null,
    updated_at: null,
    deleted_at: null, 
  },
};

const workerReducer = (state = initialState, action) => {

  switch (action.type) {
    case SET_WORKER:
      return { ...state, worker: action.worker };

    default:
      return state;
  }

}


export const setWorker = (worker) => ({ type: SET_WORKER, worker });

export const requestWorker = (workerId) => {
  return async (dispatch) => {
    let response = await apiGetWorker(workerId);
    dispatch(setWorker(response.data));
  }
}

export default workerReducer;