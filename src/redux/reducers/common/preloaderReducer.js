const LOADER_STATE_CHANGE = 'LOADER_STATE_CHANGE';

let initialState = {
  loaderCondition: false,
};

const preloaderReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOADER_STATE_CHANGE:
      return {...state, loaderCondition: action.isLoading}

    default:
      return state;
  }
}

export const changeLoaderCondition = (isLoading) => ({type: LOADER_STATE_CHANGE, isLoading});

export default preloaderReducer;