import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './redux/store.js';
import Defender from './Defender';
import {createBrowserHistory} from 'history';

const history = createBrowserHistory();


ReactDOM.render(
  <Router>
    <Provider history={history} store={store}>
      <Defender />
    </Provider>
  </Router >,
  document.getElementById('root')
);

