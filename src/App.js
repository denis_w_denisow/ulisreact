import React, { Suspense, lazy } from 'react';
import { Route } from 'react-router-dom';
import Header from './components/Header/Header';

import WorkerContainer from './components/Worker/WorkerContainer';
import CabinetsContainer from './components/Cabinets/CabinetsContainer';
import LogoutContainer from './components/Login/LogoutContainer';
import GraphicsContainer from './components/Graphics/GraphicsContainer';

const Home = lazy(() => import('./components/Home/Home'))
const ApplicationsContainer = lazy(() => import('./components/ApplicationsContainer/ApplicationsContainer'))
const WorkersContainer = lazy(() => import('./components/Workers/WorkersContainer'))

const App = ({ history }) => {
  return (
    <div>
      <Header />
      <div className="content">
        <Route exact path='/logout' component={LogoutContainer} />
        <Route exact path='/' render={() => {
          return (
            <Suspense fallback={<div>...Loading</div>}>
              <Home />
            </Suspense>
          )
        }} />
        <Route exact path='/applications' render={() => {
          return (
            <Suspense fallback={<div>...Loading</div>}>
              <ApplicationsContainer />
            </Suspense>
          )
        }} />
        <Route path='/workers' render={() => {
          return (
            <Suspense fallback={<div>...Loading</div>}>
              <WorkersContainer />
            </Suspense>
          )
        }} />
        <Route path='/worker/:uid?' render={(match) => <WorkerContainer />} />
        <Route exact path='/cabinets' component={CabinetsContainer} />
        <Route exact path='/graphics' component={GraphicsContainer} />
      </div>
    </div>
  )
}

export default App;
