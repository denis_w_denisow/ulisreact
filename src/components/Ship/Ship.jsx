import React from 'react';
import s from './Ship.module.css';

const Ship = (props) => {

  let day_list = props.data.shipPage.shipDays
    .map((dayObj, index) => {
      return (
        <th key={index} className={s[dayObj.workTypeCss]}>
          {dayObj.dateDay}<br />
          {dayObj.weekDayName}
        </th>
      )
    });



  return (
    <div className={s.shipBlock}>
      <h1>uid:{props.data.graphicsPage.graphics[3].uid} - {props.data.graphicsPage.graphics[3].name}</h1>
      <div className={s.shipBlockTable}>
        <table id="table_ship">
          <thead>
            <tr>
              <th>ФИО</th>
              {day_list}
            </tr>
          </thead>
          <tbody>

          </tbody>
        </table>
      </div>
    </div>
  );
}

export default Ship;