import React, { useState, useEffect } from 'react';
import Preloader from '../common/Preloader/Preloader';
import Pagination from '../common/Pagination/Pagination';
import WorkerLine from './WorkerLine/WorkerLine';
import { connect } from 'react-redux';
import { requestWorkers, setCurrentPage } from '../../redux/reducers/workersReducer';
import { getWorkers, getTotal, getCurrentPage } from '../../redux/selectors/workersSelector';
import TitlePage from '../common/TitlePage/TitlePage';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import ButtonBar from '../common/ButtonBar/ButtonBar';





const Workers = ({ currentPage, setCurrentPage, workers, total, requestWorkers }) => {

  const [selectValue, setSelectValue] = useState(5);
  const [loaderCondition, toggleLoaderCondition] = useState(true);

  useEffect(() => {
    toggleLoaderCondition(true);
    requestWorkers(selectValue, currentPage)
      .then(() => {
        toggleLoaderCondition(false);
      });
  }, [currentPage, selectValue, requestWorkers]);

  return (
    <div id="users" className="users">

      <Preloader loaderCondition={loaderCondition} />

      <TitlePage>
        <h1>Список сотрудников</h1>
        <ButtonBar>
          <button>Новый сотрудник</button>
        </ButtonBar>
      </TitlePage>



      <Pagination
        total={total}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        selectValue={selectValue}
        setSelectValue={setSelectValue}
      />

      <div>
        {workers.map((worker) => <WorkerLine key={worker.id} worker={worker} />)}
      </div>

    </div>
  );
}


let mapStateToProps = (state) => ({
  currentPage: getCurrentPage(state),
  workers: getWorkers(state),
  total: getTotal(state),
})

const WorkersContainer = compose(
  withRouter,
  connect(mapStateToProps, {
    requestWorkers,
    setCurrentPage
  })
)(Workers);

export default WorkersContainer;