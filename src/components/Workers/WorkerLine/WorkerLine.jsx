import React from 'react';
import s from './WorkerLine.module.css';
import { NavLink } from 'react-router-dom';

const WorkerLine = (props) => {

  return (
    <div className={s.worker}>
      <div className={s.worker_name}>
        <NavLink to={`/worker/${props.worker.id}`}>{props.worker.surname} {props.worker.name} {props.worker.patronymic}</NavLink>
      </div>
      <div className={s.worker_position}>{props.worker.position.name}</div>
      <div><button onClick={() => alert("graphic")} >График</button></div>
    </div>
  );
}

export default WorkerLine;