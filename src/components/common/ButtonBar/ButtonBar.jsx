import React from 'react'
import s from './ButtonBar.module.css'

const ButtonBar = ({ children }) => {
  return (
    <div className={s.buttonsBar}>
      { children }
    </div>
  )
}

export default ButtonBar
