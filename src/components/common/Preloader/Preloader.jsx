import React from 'react';
import loader from './img/load.svg';
import s from './Preloader.module.css';

const Preloader = (props) => {
  if (props.loaderCondition) {
    return (
      <div className={s.preloader}>
        <img className={s.loader} src={loader} alt="лоадер"/>
      </div>
    )
  }
  return '';
};

export default Preloader;