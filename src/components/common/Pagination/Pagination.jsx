import React, { useState } from 'react';
import s from './Pagination.module.css';

const Button = ({ currentPage, page, setCurrentPage }) => {
  return (
    <span
      className={`${s.pageButton} ${currentPage === page ? s.active : ''}`}
      onClick={() => setCurrentPage(page)}
    >
      {page}
    </span>
  )
}

const Option = ({ index, value }) => {
  return <option key={index} value={value}>{value}</option>
}

const Select = ({ selectValue, selectOption, onChange, title }) => {
  return (
    <select value={selectValue} className={s.selectCountRecord} onChange={(e) => onChange(e.target.value)} title={title}>
      {selectOption.map((value, index) => {
        return <Option key={index} value={value} />
      })}
    </select>
  )
}

const ShiftButtun = ({ name, onClick, title }) => {
  return <button className={s.button} onClick={onClick} title={title}>{name}</button>
}

const Paginate = ({ total, currentPage, setCurrentPage, selectValue, setSelectValue }) => {

  const [currentPageButtonBlock, setCurrentPageButtonBlock] = useState(1);
  const [pageButtonBlockParam, setPageButtonBlockParam] = useState(30);
  const paginateButtons = [];
  const countPageButtons = Math.ceil(total / selectValue);
  const countPageButtonBlock = Math.ceil(countPageButtons / pageButtonBlockParam);
  const firstButtonInBlock = currentPageButtonBlock * pageButtonBlockParam - pageButtonBlockParam + 1;
  let lastButtonInBlock = currentPageButtonBlock * pageButtonBlockParam;

  if (lastButtonInBlock > countPageButtons) {
    lastButtonInBlock = countPageButtons;
  }

  for (let p = firstButtonInBlock; p <= lastButtonInBlock; p++) {
    paginateButtons.push(p);
  }

  const goToButonBlock = (numberButtonBlock) => {
    return setCurrentPageButtonBlock(() => numberButtonBlock);
  }

  const changeCurrentPageButtonBlock = (margin) => {
    return setCurrentPageButtonBlock((prev) => prev + margin);
  }

  const changePageButtonBlockParam = (newParam) => {
    return setPageButtonBlockParam(() => newParam);
  }

  return (
    <div className={s.pagination}>      
      <Select
        selectValue={selectValue}
        selectOption={[5, 10, 20, 50, 100]}
        onChange={setSelectValue}
        title="Количество записей"
      />
      { currentPageButtonBlock > 1 ? <ShiftButtun name="<<" onClick={() => goToButonBlock(1)} title="В начало"/> : '' }
      { currentPageButtonBlock > 1 ? <ShiftButtun name="<" onClick={() => changeCurrentPageButtonBlock(-1)} title="Шаг назад" /> : '' }
      { paginateButtons.map((page) => <Button key={page} currentPage={currentPage} page={page} setCurrentPage={setCurrentPage} />) }
      { currentPageButtonBlock < countPageButtonBlock ? <ShiftButtun name=">" onClick={() => changeCurrentPageButtonBlock(1)} title="Шаг вперед" /> : '' }
      { currentPageButtonBlock < countPageButtonBlock ? <ShiftButtun name=">>" onClick={() => goToButonBlock(countPageButtonBlock)} title="В конец" /> : '' }
      <Select
        selectValue={pageButtonBlockParam}
        selectOption={[5, 10, 20, 30]}
        onChange={changePageButtonBlockParam}
        title="Количество кнопок"
      />      
      <span className={s.total}>Всего: {total}</span>
    </div>
  );
}

export default Paginate;