import React, { useState, useEffect } from 'react';
import s from './Period.module.css';

const Period = ({onChangePeriod}) => {

  function formatMonth(month) {
    return month < 10 ? '0' + month : month;
  }

  function formatDay(day) {
    return day < 10 ? '0' + day : day;
  }

  let currentDate = new Date(); 
  let currentYear =  currentDate.getFullYear();
  let currentMonth = currentDate.getMonth()+1;
  let firstDay = 1;
  let lastDay = new Date(currentYear, currentMonth, 0).getDate();
  let currentFirstDate = `${currentYear}-${formatMonth(currentMonth)}-${formatDay(firstDay)}`;
  let currentLastDate = `${currentYear}-${formatMonth(currentMonth)}-${lastDay}`;

  let [ firstDate, changeFirstDate ] = useState(currentFirstDate);
  let [ lastDate, changeLastDate ] = useState(currentLastDate);

  useEffect(() => {
    console.log('RENDER in PERIOD');
    onChangePeriod({
      firstDate, 
      lastDate
    });
  }, [firstDate, lastDate, onChangePeriod]);

  const onChangeFirstDate = (e) => {
    let newFirstDate = e.target.value;
    if (newFirstDate > lastDate) {
      changeLastDate(newFirstDate);
    }
    changeFirstDate(newFirstDate);
  }

  const onChangeLastDate = (e) => {
    let newLastDate = e.target.value;
    if (newLastDate < firstDate) {
      changeFirstDate(newLastDate);
    }
    changeLastDate(newLastDate);
  }

  return (
    <div className={s.period}>
      <span>Период:</span> c <input type="date" value={firstDate} onChange={(e) => onChangeFirstDate(e)} /> по <input value={lastDate} onChange={(e) => onChangeLastDate(e)} type="date" />
    </div>
  )
}

export default Period;