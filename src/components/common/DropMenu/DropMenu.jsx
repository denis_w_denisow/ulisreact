import React, { useState } from 'react'
import s from './DropMenu.module.css'

const SubmenuItem = ({ child }) => {
  return (
    <li>
      {child}
    </li>
  )
}

const DropMenu = ({ name, children, ...attrs }) => {

  let [submenuCondition, toggleSubmenuCondition] = useState(false);

  const toggleDropMenu = () => {
    toggleSubmenuCondition((prev) => {
      return !prev
    })
    console.log('change dm')
  }

  let submenuClasses = s.submenu
  if (submenuCondition) {
    submenuClasses = `${s.submenu} ${s.show}`
  }

  return (
    <div {...attrs} className={s.dropMenuBox}>
      <span onClick={toggleDropMenu} >{name}</span>
      <ul className={submenuClasses}>
        {React.Children.toArray(children).map((child, index) => {
          return <SubmenuItem key={index} child={child} />
        })}
      </ul>
    </div>
  )
}

export default DropMenu