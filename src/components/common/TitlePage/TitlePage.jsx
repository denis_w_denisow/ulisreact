import React from 'react'
import s from './TitlePage.module.css'

const TitlePage = ({ children }) => {
  return (
    <div className={s.titlePage}>
      { children }
    </div>
  )
}

export default TitlePage;