import React from 'react';

export const DateConverter = ({ dateValue }) => {

  let currentDate = new Date();
  if (dateValue) {
    currentDate = new Date(dateValue);
  }
  
  function formatMonth(month) {
    return month < 10 ? '0' + month : month;
  }

  function formatDay(day) {
    return day < 10 ? '0' + day : day;
  }

  let currentYear =  currentDate.getFullYear();
  let currentMonth = currentDate.getMonth()+1;
  let firstDay = currentDate.getDate();

  return (
    <>
      {`${formatDay(firstDay)}.${formatMonth(currentMonth)}.${currentYear}`}
    </>
  )
}