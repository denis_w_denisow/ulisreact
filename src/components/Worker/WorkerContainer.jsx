import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import TitlePage from '../common/TitlePage/TitlePage';
import { requestWorker } from '../../redux/reducers/workerReducer';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import s from './Worker.module.css';
import Period from '../common/Period/Period';



const Worker = ({ match, worker, requestWorker }) => {

  const onChangePeriod = (obj) => {
    console.log(obj); 
  }

  useEffect(() => {
    requestWorker(match.params.uid)
  }, [match.params.uid, requestWorker])

  return (
    <div>

      <TitlePage name="Сотрудник" />

      <div className={s.head}>
        <div className={s.worker_name}>{worker.surname} {worker.name} {worker.patronymic}</div>
        <div className={s.worker_position}>{worker.position.name}</div>
        <div className={s.worker_cabinet}>{worker.cabinet.name}</div>
      </div>
      
      <Period onChangePeriod={ onChangePeriod } />


    </div>
  )

}

let mapStateToProps = (state) => {

  return {
    worker: state.workerReducer.worker,
  }
}

const WorkerContainer = compose(
  withRouter,
  connect(mapStateToProps, {
    requestWorker
  })
)(Worker);

export default WorkerContainer