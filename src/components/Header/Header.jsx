import React from 'react';
import s from './Header.module.css'
import logo from './logo.svg';
import Linkbar from './Linkbar/Linkbar';
import DropMenu from '../common/DropMenu/DropMenu';
import { NavLink } from 'react-router-dom';

const Header = () => {

  const profile = JSON.parse(localStorage.getItem('worker'));

  return (
    <div className={s.App}>
      <header className={s.App_header}>
        <img src={logo} className={s.App_logo} alt="logo" />
        <Linkbar />
        <div className={s.right}>
          <DropMenu name={`${profile.surname} ${profile.name}`}>            
              <NavLink to="/" >Изменить кабинет</NavLink>
              <NavLink to="/2" >Изменить пароль</NavLink>
              <NavLink to="/logout" >Выход</NavLink>           
          </DropMenu>
        </div>
      </header>
    </div>
  );
}

export default Header;