import React, { useEffect } from 'react';
import s from './Linkbar.module.css';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { requestMenu } from '../../../redux/reducers/menuReducer'

const Link = ({link, name}) => {
  return (
    <NavLink exact to={link} activeClassName={s.active}>
      {name}
    </NavLink>
  )
}

const Linkbar = ({menu, requestMenu}) => {

  useEffect(() => {
    requestMenu();
  }, [requestMenu])

  return (
    <div className={s.linkbar}>
      {
        menu.map((item) => {
          return <Link key={item.id} link={item.link} name={item.name} />
        })
      }      
      
    </div>
  )
}

let mapStateToProps = (state) => ({
  menu: state.menuReducer.menu
}) 

export default connect(mapStateToProps, {
  requestMenu
})(Linkbar);