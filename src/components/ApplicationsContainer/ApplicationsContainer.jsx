import React, { useState, useEffect } from 'react';
import { getApplications, getTotalRecords } from '../../redux/selectors/applicationsSelector';
import { requestApplications } from '../../redux/reducers/applicationsReducer';
import { connect } from 'react-redux';
import Preloader from '../common/Preloader/Preloader';
import TitlePage from '../common/TitlePage/TitlePage';
import Pagination from '../common/Pagination/Pagination';
import ApplicationLine from './ApplicationLine';
import ButtonBar from '../common/ButtonBar/ButtonBar';


const Applications = ({ applications, total, requestApplications }) => {

  const [currentPage, setCurrentPage] = useState(1);
  const [selectValue, setSelectValue] = useState(100);
  const [loaderCondition, toggleLoaderCondition] = useState(true);
  const [search, setSearch] = useState('');
  const [requestSearch, setRequestSearch] = useState('');

  const changeSearchValue = (e) => {
    const word = e.target.value;
    setSearch(() => word);
    if (word.length > 2) {
      setRequestSearch(() => word);
    }
  }

  useEffect(() => {
    toggleLoaderCondition(true);
    requestApplications(requestSearch, selectValue, currentPage)
      .then(() => {
        toggleLoaderCondition(false);
      });
  }, [requestSearch, currentPage, selectValue, requestApplications]);

  return (
    <div>
      <Preloader loaderCondition={loaderCondition} />

      <TitlePage>
        <h1>Список обращений</h1>
        <ButtonBar>
          <button>Новое обращение</button>
        </ButtonBar>
      </TitlePage>

      <Pagination
        total={total}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        selectValue={selectValue}
        setSelectValue={setSelectValue}
      />

      <div className="container">
        <input className="form-control" type="text" placeholder="Поиск по номеру или заявителю" value={search} onChange={(e) => changeSearchValue(e)} />
      </div>

      <table className="table table-dark">
        <thead>
          <tr>
            <th>UID</th>
            <th>Регистрационный номер</th>
            <th>Заявитель</th>
            <th>Услуга</th>
            <th>Размещение</th>
            <th>Статус</th>
            <th>Объект</th>
            <th>Дата</th>
            <th>Действия</th>
          </tr>
        </thead>
        <tbody>
          {(applications.length === 1 && applications[0].id === 0) || (applications.length === 0) ?
            <tr >
              <td colSpan="9">записи отсутствуют</td>
            </tr>
            :
            applications.map((application) => {
              return <ApplicationLine key={application.id} application={application} />
            })
          }
        </tbody>
      </table>
    </div>
  )
}

let mapStateToProps = (state) => {
  return {
    applications: getApplications(state),
    total: getTotalRecords(state),
  }
}

const ApplicationsContainer = connect(mapStateToProps, {
  requestApplications,
})(Applications);

export default ApplicationsContainer;