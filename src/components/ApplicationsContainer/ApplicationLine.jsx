import React from 'react'
import { NavLink } from 'react-router-dom';

const Action = ({ action, application_id, onHandleClick}) => {

  let className = `btn btn-default`
  if (action.action_css_name) {
    className = `btn btn-${action.action_css_name}`
  }

  return (
    <button className={className} title={action.action_name} data-application_id={application_id} data-action_id={action.action_id} onClick={()=>onHandleClick(application_id)}>
      {action.action_name}
    </button>
  )
}

const ApplicationLine = ({application}) => {

  const onActionClick = (application_id) => {
    alert(application_id)
    console.log(this)
  }

  return (
    <tr>
      <td>{application.id}</td>
      <td><NavLink to={`/application/${application.id}`}>{application.registration_number}</NavLink></td>
      <td>{application.registration_name ? application.registration_name : 'Не указан'}</td>
      <td>{application.service_name}</td>
      <td>{application.cabinet_name}</td>
      <td><span className={`label label-${application.status_css_name}`}>{application.status_name}</span></td>
      <td><span className={`label label-${application.conversion_css_name}`}>{application.conversion_name}</span></td>
      <td>{application.date_format}</td>
      <td>{
        application.actions.map((action, index) => {
          if (action.action_id > 0) {
            return <Action key={index} action={action} application_id={application.id} onHandleClick={onActionClick} />
          }
          return '';
        })}</td>
    </tr>
  )
}

export default ApplicationLine