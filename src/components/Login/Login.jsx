import React from 'react';
import { reduxForm, Field } from 'redux-form';
import s from './Login.module.css'

const Form = (props) => {
  return (
    <form onSubmit={props.handleSubmit}>

      <div className={s.inputIteam}>
        <label htmlFor="login">E-mail:</label>
        <Field name="email" component="input" type="email" placeholder="Логин" />
      </div>

      <div className={s.inputIteam}>
        <label htmlFor="password">Пароль:</label>
        <Field name="password" component="input" type="password" placeholder="Пароль" />
      </div>

      <div className={s.buttonIteam}>
        <div>
          
        </div>
        <div>
          <Field className={s.buttonIn} name="save" component="button">Войти</Field>
        </div>
      </div>

    </form>
  )
}

const Login = (props) => {
  
  const onSubmit = (values) => {
    console.log('start');
    props.getAccessToken(values.email, values.password);

  }

  return (
    <div className={s.login_block}>
      <h1>Вход в приложение:</h1>
      <LoginForm onSubmit={onSubmit.bind(this)} />
    </div>
  )
}

const LoginForm = reduxForm({
  form: 'login'
})(Form);

export default Login