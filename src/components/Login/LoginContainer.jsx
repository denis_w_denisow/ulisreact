import { connect } from 'react-redux';
import Login from './Login';
import { getAccessToken } from '../../redux/reducers/loginReducer';

let mapStateToProps = (state) => ({
  emal: state.loginReducer.emal,
  password: state.loginReducer.password,
  access_token: state.loginReducer.access_token,
  refresh_token: state.loginReducer.refresh_token,
  token_type: state.loginReducer.token_type,
  expires_in: state.loginReducer.expires_in,
  authorized: state.loginReducer.authorized
});

const LoginContainer = connect(mapStateToProps, {
  getAccessToken
})(Login)

export default LoginContainer;