import React from 'react';
import { connect } from 'react-redux';
import { logout } from '../../redux/reducers/loginReducer';

class Logout extends React.Component {
  componentDidMount() {
    this.props.logout();
  }

  render () {
    return <div></div>
  }
}

const LogoutContainer = connect(null, {
  logout
})(Logout)

export default LogoutContainer;