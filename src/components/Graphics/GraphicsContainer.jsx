import React, { useEffect, useState } from 'react';
import Preloader from '../common/Preloader/Preloader';
import Pagination from '../common/Pagination/Pagination';
import { connect } from 'react-redux';
import { requestGraphics } from '../../redux/reducers/graphicsReducer'
import { getGraphics, getTotal } from '../../redux/selectors/graphicsSelector';
import GraphicLine from './GraphicLine/GraphicLine';
import TitlePage from '../common/TitlePage/TitlePage';




const Graphics = ({graphics, total, requestGraphics}) => {

  const [currentPage, setCurrentPage] = useState(1);
  const [selectValue, setSelectValue] = useState(5);
  const [loaderCondition, toggleLoaderCondition] = useState(true);

  useEffect(() => {
    toggleLoaderCondition(true);
    requestGraphics(selectValue, currentPage)
    .then(() => {
      toggleLoaderCondition(false);
    });    
  }, [currentPage, selectValue, requestGraphics]);

  return (
    <div>  

      <Preloader loaderCondition={loaderCondition} />

      <TitlePage name="Список графиков" />

      <Pagination 
        total={total} 
        currentPage={currentPage} 
        setCurrentPage={setCurrentPage}
        selectValue={selectValue}
        setSelectValue={setSelectValue}
      />     

      {
        graphics.map((graphic) => {
          return <GraphicLine key={graphic.id} graphic={graphic} />
        })
      }

    </div>
  );
}

let mapStateToProps = (state) => ({
  graphics: getGraphics(state),
  total: getTotal(state),
})

const GraphicsContainer = connect(mapStateToProps, {
  requestGraphics,
})(Graphics);

export default GraphicsContainer;