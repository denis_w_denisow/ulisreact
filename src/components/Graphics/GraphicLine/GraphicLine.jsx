import React from 'react';
import s from './GraphicLine.module.css';
import { NavLink } from 'react-router-dom';

const GraphicLine = ({ graphic }) => {
  return (
    <div className={s.graphic} >
      <div className={s.graphic_name}>
        <NavLink to={`/graphic/${graphic.id}`}>{graphic.name}</NavLink>
      </div>
      <div>
        {graphic.firstDay} - {graphic.lastDay}
      </div>
    </div>
  )
}

export default GraphicLine;