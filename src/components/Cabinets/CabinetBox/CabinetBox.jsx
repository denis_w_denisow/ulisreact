import React, {useState} from 'react';
import s from './CabinetBox.module.css';

const Arm =({arm}) => {

  let [isShow, toggleShov] = useState(false);

  let active = isShow ? s.active : '';
  let pres = isShow ? s.pres : '';

  const showPopap = () => {
    toggleShov(true);
  }

  const hidePopap = () => {
    toggleShov(false);
  }

  return (
    <div className={s.armsBox}>
      <div className={`${s.arm} ${pres}`} onMouseOverCapture={showPopap} onMouseOut={hidePopap}>{arm.id}</div>
      <div className={`${s.popap} ${active}`}>
        <div><b>UID: </b>{arm.id}</div>  
        <div><b>Условное название: </b></div>{arm.name}
        <div><b>Описание: </b></div>{arm.description}  
      </div>
        
    </div>
  )
}

const CabinetBox = ({ cabinet }) => {
  return (
    <div className={s.cabinetBox}>
      
      <div><b>Наименование: </b>{cabinet.name}</div>
      <div><b>Адрес: </b>{cabinet.address}</div>
      <div><b>АРМ: </b>{cabinet.arms.length}</div>
      <div className={s.armsBox}>
        {
          cabinet.arms.map((arm) => {
            return <Arm key={arm.id} arm={arm} />
          })
        }        
      </div>
    </div>
  )
}

export default CabinetBox;

