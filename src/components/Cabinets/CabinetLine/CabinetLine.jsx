import React from 'react'
import s from './CabinetLine.module.css'
import { NavLink } from 'react-router-dom';

class CabinetLine extends React.Component {

  state = {
    editModeAddress: false,
  }

  onClickAddres = () => {
    this.setState({ editModeAddress: true });
  }

  onBlurAddress = (cabinetId, address) => {
    this.setState({ editModeAddress: false });
    this.props.saveCabinetAddress(cabinetId, address);
  }

  render() {
    return (
      <div key={this.props.cabinet.id} className={s.cabinet} >
        <div className={s.cabinet_name}>
          <NavLink to={`/cabinet/${this.props.cabinet.id}`}>{this.props.cabinet.name}</NavLink>
        </div>
        <div onClick={this.onClickAddres} className={s.cabinet_address}>
          {!this.state.editModeAddress && <span>{this.props.cabinet.address || 'адрес не указан'}</span>}
          {this.state.editModeAddress && <input onChange={(event) => this.props.setCabinetAddress(this.props.cabinet.id, event.target.value)} onBlur={(e) => this.onBlurAddress(this.props.cabinet.id, e.target.value)} autoFocus={true} value={this.props.cabinet.address} />}
        </div>
        <div>АРМ: {this.props.cabinet.arms.length}</div>
      </div>
    )
  }
}

export default CabinetLine;