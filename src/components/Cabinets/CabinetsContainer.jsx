import React, { useEffect, useState } from 'react';
import Preloader from '../common/Preloader/Preloader';
import Pagination from '../common/Pagination/Pagination';
import { connect } from 'react-redux';
import { requestCabinets, setCabinetAddress, saveCabinetAddress } from '../../redux/reducers/cabinetsReducer'
//import CabinetLine from './CabinetLine/CabinetLine';
import { getCabinets, getTotal } from '../../redux/selectors/cabinetsSelector';
import TitlePage from '../common/TitlePage/TitlePage';
import CabinetBox from './CabinetBox/CabinetBox';
import s from './CabinetsContainer.module.css';
import ButtonBar from '../common/ButtonBar/ButtonBar';

const Cabbinets = ({ cabinets, total, requestCabinets, setCabinetAddress, saveCabinetAddress }) => {

  const [currentPage, setCurrentPage] = useState(1);
  const [selectValue, setSelectValue] = useState(5);
  const [loaderCondition, toggleLoaderCondition] = useState(true);

  useEffect(() => {
    toggleLoaderCondition(true);
    requestCabinets(selectValue, currentPage)
      .then(() => {
        toggleLoaderCondition(false);
      });
  }, [currentPage, selectValue, requestCabinets]);

  return (
    <div>

      <Preloader loaderCondition={loaderCondition} />

      <TitlePage>
        <h1>Список кабинетов</h1>
        <ButtonBar>
          <button>Новый кабинет</button>
        </ButtonBar>
      </TitlePage>

      <Pagination
        total={total}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        selectValue={selectValue}
        setSelectValue={setSelectValue}
      />

      <div className={s.cabinets}>
        {cabinets.map((cabinet) => {
          return (
            <CabinetBox
              key={cabinet.id}
              cabinet={cabinet}
              setCabinetAddress={setCabinetAddress}
              saveCabinetAddress={saveCabinetAddress}
            />
          )
        })}
      </div>

    </div>
  );
}


let mapStateToProps = (state) => {
  return {
    cabinets: getCabinets(state),
    total: getTotal(state),
  }
}

const CabintesContainer = connect(mapStateToProps, {
  requestCabinets,
  setCabinetAddress,
  saveCabinetAddress,
})(Cabbinets);

export default CabintesContainer;