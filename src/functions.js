/**
 * Возвращает объект сотрудника из массива users
 * или undefined
 * 
 * @param {1} id 
 */
function findUserById(users, id) {
  return users.find(function (iteam) {
      return iteam.uid === id;
  });
}

// Список всех дней в заданом графике работы
/**
    [
    0: {curDate: "2020-09-01", day: "01", mon: "09", year: 2020, type: 2},
    1: {curDate: "2020-09-02", day: "02", mon: "09", year: 2020, type: 3},
    ...
    29: {curDate: "2020-09-30", day: 30, mon: "09", year: 2020, type: 3}
    length: 30
    __proto__: Array(0)
    ]
 */
function getDays(ship) {
  let firstDay = new Date(ship.firstDay);
  let lastDay = new Date(ship.lastDay);
  let days = [];
  //let countDays = (lastDay - firstDay) / 86400000;
  for (let day = firstDay; day <= lastDay; day.setDate(day.getDate() + 1)) {
      days.push({
          curDate: `${day.getFullYear()}-${(day.getMonth() + 1) < 10 ? '0' + (day.getMonth() + 1) : (day.getMonth() + 1)}-${day.getDate() < 10 ? '0' + day.getDate() : day.getDate()}`,
          day: day.getDate() < 10 ? '0' + day.getDate() : day.getDate(),
          mon: (day.getMonth() + 1) < 10 ? '0' + (day.getMonth() + 1) : (day.getMonth() + 1),
          year: day.getFullYear(),
          type: day.getDay(),
      });
  }
  return days;
}

/**
 * День недели в зависимости от номера
 * 0 - вс, 1 - пн, ..., 6 - сб
 * @param {0-6} dayNumber 
 */
function getWeekday(dayNumber) {
  let weekName = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'];
  return weekName[dayNumber];
}

/**
 * Возвращает объект - рабочегий днь 
 * сотрудника из массива workLists объекта user
 * по id сотрудника и по дате
 * или undefined
 * 
 * @param {1} userUid 
 * @param {'2020-09-01'} day 
 */
function getUserOneWorkDay(users, userUid, day) {
  return findUserById(users, userUid).workLists.find(function (iteam) {
      return iteam.day === day;
  });
}

/**
 * Возвращает массив кабинетов одного сотрудника за один день
 * 
 * @param {1} userUid 
 * @param {'2020-09-01'} day 
 */
function getUserOneWorkDayCabinets(users, cabinets, userUid, day) {
  let workDay = getUserOneWorkDay(users, userUid, day);
  if (workDay) {
      return workDay.cabinets.map(function (cabinet) {
          return {
              cabinet: getCabinetById(cabinets, cabinet.cabinetId),
              workTime: cabinet.workTime,
          }
      });
  }
}

/**
 * Возвращает объект все кабинеты сотрудника за конкретный день
 * {
 *  cabinetName: "Орехово Окно 1 ", - склееное название кабинетов за один день
 *  errors: 0, - расхождение от нормативного времени работы сотрудника (- переработка, + недоработка)
 *  sumWorkTime: 8, - сумма рабочего времени в кабинетах
 * }
 * 
 * @param {1} userUid 
 * @param {'2020-09-01'} day 
 */
function makeSumWorkTimeOneDay(cabinets, users, userUid, day) {
  let workDay = getUserOneWorkDay(users, userUid, day);
  let workDayCabinets = getUserOneWorkDayCabinets(users, cabinets, userUid, day);
  if (workDayCabinets) {
      let cabinetName = '';
      let sumWorkTime = 0;
      let normalWorkTime = getWorkHours(workDay.startWork, workDay.endWork) - getWorkHours(workDay.lunchBreak.start, workDay.lunchBreak.end);
      let prevCabinetName = '';
      for (let cabinet of workDayCabinets) {
          if (prevCabinetName !== cabinet.cabinet.name) {
              cabinetName += `${cabinet.cabinet.name} `;
          }
          prevCabinetName = cabinet.cabinet.name
          sumWorkTime += getWorkHours(cabinet.workTime.start, cabinet.workTime.end);
      }
      return {
          cabinetName,
          sumWorkTime,
          errors: normalWorkTime - sumWorkTime,
      }
  }
}

/**
 * Возвращает оъект кабинета по uid
 * @param {1} id 
 */
function getCabinetById(cabinets, id) {
  return cabinets.find(function (cabObj) {
      return cabObj.uid === id;
  });
}


let getWorkHours = (timeStart, timeEnd) => {
  if (timeStart && timeEnd) {
    let startParam = timeStart.split(':');
    let start = new Date();
    start.setHours(startParam[0], startParam[1], startParam[2]);
    let endParam = timeEnd.split(':');
    let end = new Date();
    end.setHours(endParam[0], endParam[1], endParam[2]);
    return Math.round((end - start) / 1000 / 3600 * 10) / 10;
  }
  return 0;
}


let calcWorkTime = (objWorkList) => {
  let startW = objWorkList.startWork;
  let enfW = objWorkList.endWork;
  let startBL = objWorkList.lunchBreak.start;
  let endLB = objWorkList.lunchBreak.end;
  return getWorkHours(startW, enfW) - getWorkHours(startBL, endLB);
}

let allUserWorkTime = (users) => {
  return users.reduce(function (prevUsers, user) {
    return prevUsers + user.workLists.reduce(function (prev, workList) {
      return prev + calcWorkTime(workList);
    }, 0);
  }, 0)
}

let sumUserWorkTime = (user) => {
  return user.workLists.reduce(function (prev, workList) {
    return prev + calcWorkTime(workList);
  }, 0)
}

export default {findUserById, getDays, getWeekday, getUserOneWorkDay, makeSumWorkTimeOneDay, getCabinetById, getWorkHours, calcWorkTime, allUserWorkTime, sumUserWorkTime}