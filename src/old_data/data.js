let data = {

  makeUserList() {


    return [
      {
        name: 'User 1',
        horesworkDays: [
          8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 0, 0, 0, 8,
        ],
      },
      {
        name: 'User 3',
        workDays: [
          8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 0, 0, 0, 8,
        ],
      },
    ]
  },

  getWeekdayName(weekdayNumber) {
    let weekdayName = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'];
    return weekdayName[weekdayNumber];
  },

  getDays() {
    let firstDay = new Date(this.graphics.graphic[4]);
    let lastDay = new Date(this.graphics.graphic[4]);
    let days = [];
    for (let day = firstDay; day <= lastDay; day.setDate(day.getDate() + 1)) {
      days.push({
        curDate: `
            ${day.getFullYear()}-
            ${(day.getMonth() + 1) < 10
            ? '0' + (day.getMonth() + 1)
            : (day.getMonth() + 1)}-
            ${day.getDate() < 10
            ? '0' + day.getDate()
            : day.getDate()}
          `,
        day: day.getDate() < 10 ? '0' + day.getDate() : day.getDate(),
        mon: (day.getMonth() + 1) < 10 ? '0' + (day.getMonth() + 1) : (day.getMonth() + 1),
        year: day.getFullYear(),
        type: day.getDay(),
      });
    }
    return days;
  },

  /**
 * Возвращает массив - все рабочие часы из user.woerLists 
 * или 0 если запись отсутствует
 * по всему массиву users
 * по всем датам из ship
 */
  getWorkerList() {
    let dayList = this.getDays();
    let result = this.users.map(function (user) {
      return {
        name: `uid: ${user.uid} - ${user.name}`,
        workerLists: user.workLists,
        workTime: dayList.map(function (dates) {
          //let userOneWorkDay = getUserOneWorkDay(user.uid, `${dates.curDate}`);
          //if (userOneWorkDay) return getWorkHours(userOneWorkDay.startWork, userOneWorkDay.endWork) - getWorkHours(userOneWorkDay.lunchBreak.start, userOneWorkDay.lunchBreak.end);
          return 0;
        }),
      };
    });
    return result;
  },

  shipPage: {
    shipDays: [
      {
        dateDay: '01.9',
        weekDayName: 'вт',
        workType: 1,
        workTypeCss: 'working',
      },
      {
        dateDay: '02.9',
        weekDayName: 'ср',
        workType: 1,
        workTypeCss: 'working',
      },
      {
        dateDay: '03.9',
        weekDayName: 'чт',
        workType: 1,
        workTypeCss: 'working',
      },
      {
        dateDay: '04.9',
        weekDayName: 'пт',
        workType: 1,
        workTypeCss: 'working',
      },
      {
        dateDay: '05.9',
        weekDayName: 'сб',
        workType: 1,
        workTypeCss: 'short',
      },
      {
        dateDay: '06.9',
        weekDayName: 'вс',
        workType: 1,
        workTypeCss: 'weekend',
      },
      {
        dateDay: '07.9',
        weekDayName: 'пн',
        workType: 1,
        workTypeCss: 'working',
      },
      {
        dateDay: '08.9',
        weekDayName: 'вт',
        workType: 1,
        workTypeCss: 'working',
      },
      {
        dateDay: '09.9',
        weekDayName: 'ср',
        workType: 1,
        workTypeCss: 'working',
      },
      {
        dateDay: '10.9',
        weekDayName: 'чт',
        workType: 1,
        workTypeCss: 'working',
      },
      {
        dateDay: '11.9',
        weekDayName: 'пт',
        workType: 1,
        workTypeCss: 'working',
      },
      {
        dateDay: '12.9',
        weekDayName: 'сб',
        workType: 1,
        workTypeCss: 'short',
      },
      {
        dateDay: '13.9',
        weekDayName: 'вс',
        workType: 1,
        workTypeCss: 'weekend',
      },
      {
        dateDay: '14.9',
        weekDayName: 'пн',
        workType: 1,
        workTypeCss: 'working',
      },
    ],
    shipUserList: [
      {
        name: 'Сотрудник 1',

      },
    ],
  },

  cabinets: [
    {
      uid: 1,
      name: 'Окно 1',
      workDays: [
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          startWork: '09:00',
          endWork: '20:00',
          lunchBreak: {
            start: '13:00',
            end: '14:00',
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          startWork: '08:00',
          endWork: '13:00',
          lunchBreak: {
            start: null,
            end: null,
          },
        },
      ],
    },
    {
      uid: 2,
      name: 'Окно 2',
      workDays: [
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '13:00',
            end: '14:00',
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '13:00',
            end: '14:00',
          },
        },
        {
          startWork: '09:00',
          endWork: '20:00',
          lunchBreak: {
            start: '14:00',
            end: '15:00',
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '13:00',
            end: '14:00',
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '13:00',
            end: '14:00',
          },
        },
        {
          startWork: '08:00',
          endWork: '13:00',
          lunchBreak: {
            start: null,
            end: null,
          },
        },
      ],
    },
    {
      uid: 3,
      name: 'Окно 3',
      workDays: [
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          startWork: '09:00',
          endWork: '20:00',
          lunchBreak: {
            start: '13:00',
            end: '14:00',
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          startWork: '08:00',
          endWork: '13:00',
          lunchBreak: {
            start: null,
            end: null,
          },
        },
      ],
    },
    {
      uid: 4,
      name: 'Окно 4',
      workDays: [
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '13:00',
            end: '14:00',
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '13:00',
            end: '14:00',
          },
        },
        {
          startWork: '09:00',
          endWork: '20:00',
          lunchBreak: {
            start: '14:00',
            end: '15:00',
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '13:00',
            end: '14:00',
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '13:00',
            end: '14:00',
          },
        },
        {
          startWork: '08:00',
          endWork: '13:00',
          lunchBreak: {
            start: null,
            end: null,
          },
        },
      ],
    },
    {
      uid: 5,
      name: 'Окно 5',
      workDays: [
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: '09:00',
          endWork: '20:00',
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: '08:00',
          endWork: '13:00',
          lunchBreak: {
            start: null,
            end: null,
          },
        },
      ],
    },
    {
      uid: 6,
      name: 'ТОСП 1',
      workDays: [
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: '08:00',
          endWork: '12:00',
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
      ],
    },
    {
      uid: 7,
      name: 'ТОСП 2',
      workDays: [
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: '08:00',
          endWork: '12:00',
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
      ],
    },
    {
      uid: 8,
      name: 'ТОСП 3',
      workDays: [
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: '08:00',
          endWork: '12:00',
          lunchBreak: {
            start: null,
            end: null,
          },
        },
        {
          startWork: null,
          endWork: null,
          lunchBreak: {
            start: null,
            end: null,
          },
        },
      ],
    },
  ],

  users: [
    {
      uid: 1,
      name: 'Сотрудник 1',
      workLists: [
        {
          day: '2020-09-01',
          cabinets: [
            {
              cabinetId: 1,
              workTime: {
                start: '08:00',
                end: '12:00',
              },
            },
            {
              cabinetId: 1,
              workTime: {
                start: '13:00',
                end: '17:00',
              },
            }
          ],
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          day: '2020-09-04',
          cabinets: [
            {
              cabinetId: 1,
              workTime: {
                start: '08:00',
                end: '12:00',
              },
            },
            {
              cabinetId: 1,
              workTime: {
                start: '13:00',
                end: '17:00',
              },
            }
          ],
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          day: '2020-09-03',
          cabinets: [
            {
              cabinetId: 1,
              workTime: {
                start: '08:00',
                end: '12:00',
              },
            },
            {
              cabinetId: 1,
              workTime: {
                start: '13:00',
                end: '17:00',
              },
            }
          ],
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
      ]
    },
    {
      uid: 2,
      name: 'Сотрудник 2',
      workLists: [
        {
          day: '2020-09-01',
          cabinets: [
            {
              cabinetId: 1,
              workTime: {
                start: '08:00',
                end: '12:00',
              },
            },
            {
              cabinetId: 1,
              workTime: {
                start: '13:00',
                end: '17:00',
              },
            }
          ],
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          day: '2020-09-04',
          cabinets: [
            {
              cabinetId: 1,
              workTime: {
                start: '08:00',
                end: '12:00',
              },
            },
            {
              cabinetId: 1,
              workTime: {
                start: '13:00',
                end: '17:00',
              },
            }
          ],
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          day: '2020-09-03',
          cabinets: [
            {
              cabinetId: 1,
              workTime: {
                start: '08:00',
                end: '12:00',
              },
            },
            {
              cabinetId: 1,
              workTime: {
                start: '13:00',
                end: '17:00',
              },
            }
          ],
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
      ]
    },
    {
      uid: 3,
      name: 'Сотрудник 3',
      workLists: [
        {
          day: '2020-09-07',
          cabinets: [
            {
              cabinetId: 1,
              workTime: {
                start: '08:00',
                end: '12:00',
              },
            },
            {
              cabinetId: 1,
              workTime: {
                start: '13:00',
                end: '17:00',
              },
            }
          ],
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          day: '2020-09-08',
          cabinets: [
            {
              cabinetId: 1,
              workTime: {
                start: '08:00',
                end: '12:00',
              },
            },
            {
              cabinetId: 1,
              workTime: {
                start: '13:00',
                end: '17:00',
              },
            }
          ],
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          day: '2020-09-10',
          cabinets: [
            {
              cabinetId: 1,
              workTime: {
                start: '08:00',
                end: '12:00',
              },
            },
            {
              cabinetId: 1,
              workTime: {
                start: '13:00',
                end: '17:00',
              },
            }
          ],
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
      ]
    },
    {
      uid: 4,
      name: 'Сотрудник 4',
      workLists: [
        {
          day: '2020-09-25',
          cabinets: [
            {
              cabinetId: 1,
              workTime: {
                start: '08:00',
                end: '12:00',
              },
            },
            {
              cabinetId: 1,
              workTime: {
                start: '13:00',
                end: '17:00',
              },
            }
          ],
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          day: '2020-09-28',
          cabinets: [
            {
              cabinetId: 1,
              workTime: {
                start: '08:00',
                end: '12:00',
              },
            },
            {
              cabinetId: 1,
              workTime: {
                start: '13:00',
                end: '17:00',
              },
            }
          ],
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
        {
          day: '2020-09-29',
          cabinets: [
            {
              cabinetId: 1,
              workTime: {
                start: '08:00',
                end: '12:00',
              },
            },
            {
              cabinetId: 1,
              workTime: {
                start: '13:00',
                end: '17:00',
              },
            }
          ],
          startWork: '08:00',
          endWork: '17:00',
          lunchBreak: {
            start: '12:00',
            end: '13:00',
          },
        },
      ]
    },
  ],

  graphicsPage: {
    graphics: [
      {
        uid: 1,
        name: 'График на Июнь',
        firstDay: '2020-06-01',
        lastDay: '2020-06-30',
      },
      {
        uid: 2,
        name: 'График на Июль',
        firstDay: '2020-07-01',
        lastDay: '2020-07-31',
      },
      {
        uid: 3,
        name: 'График на Август',
        firstDay: '2020-08-01',
        lastDay: '2020-08-31',
      },
      {
        uid: 4,
        name: 'График на Сентябрь',
        firstDay: '2020-09-01',
        lastDay: '2020-09-30',
      },
    ]
  }


}

export default data;