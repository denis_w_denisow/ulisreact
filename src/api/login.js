import * as axios from 'axios';
import { client_id, client_secret } from './api'


export const instanceLogin = axios.create({
  withCredentials:true,
  baseURL: "http://127.0.0.1:8000/"
});


export const loginApi = (email, password) => {
  
  return instanceLogin.post(
    `oauth/token`,
    {
      grant_type: 'password',
      client_id: client_id,
      client_secret: client_secret,
      username: email,
      password: password
    }
  );
}

export const getUserProfileApi = (access_token) => {  
  return axios.get(`http://127.0.0.1:8000/api/get_user_profile`, {
    withCredentials:true,
    headers: {'Authorization': `Bearer ${access_token}`}
  })
    .then((response) => response);
}