import { instance } from "./api"


export const apiGetMenu = () => {
  return instance.get(`get_menu`)
    .then((response) => response);
}