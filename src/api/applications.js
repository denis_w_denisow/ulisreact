import { instance } from "./api"


export const apiGetApplications = (countPage, currentPage) => {
  return instance.get(`applications?countPage=${countPage}&page=${currentPage}`)
    .then((response) => response);
}

export const apiGetData = (search, countPage, currentPage,) => {
  return instance.post(`app_data`,
  {
    search: search,
    start: currentPage,
    length: countPage,
  }).then((response) => response);
}