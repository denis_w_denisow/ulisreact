import {instance} from "./api"

export const apiGetWorkers = (countPage, currentPage) => {
  return instance.get(`workers?countPage=${countPage}&page=${currentPage}`)
    .then((response) => response.data);
}

export const apiGetWorker = (workerId) => {
  return instance.get(`worker/${workerId}`)
}