import * as axios from 'axios';

export const client_id = 2;
export const client_secret = '9LAzfyDecMOVBDOTVTRrGz2IsmAhwnvSDq8MUWZc';

const access_token = localStorage.access_token;

export const instance = axios.create({
  withCredentials:true,
  baseURL: "http://127.0.0.1:8000/api/",
  headers: {'Authorization': `Bearer ${access_token}`}
});