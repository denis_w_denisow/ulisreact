import {instance} from "./api"


export const apiGetCabinets = (countPage, currentPage) => {
  return instance.get(`cabinets?countPage=${countPage}&page=${currentPage}`)
    .then((response) => response.data);
}

export const apiSaveCabinetAddress = (cabinet_id, address) => {
  return instance.post(`cabinet/save_address`, {
    cabinet_id,
    address
  });
}